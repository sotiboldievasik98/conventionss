![](conn.PNG)
### Comparison
```
data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : 
Comparable<MyDate> {
    override fun compareTo(other: MyDate) = when { 
        year != other.year -> year - other.year;
        month != other.month -> month - other.month;
        else -> dayOfMonth - other.dayOfMonth;
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1.compareTo (date2))
}}
```
### Ranges
```
fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last 
}
```
### For loop
```
class DateRange(val start: MyDate, val end: MyDate): Iterable<MyDate>{
    override fun iterator(): Iterator<MyDate> {
        return object:Iterator<MyDate>{
            var result=start
            override fun hasNext(): Boolean {
                if(result<=end){
                    return true
                }
                else return false
            }

            override fun next(): MyDate {
                var nextDay=result;
                result=result.followingDate();
                return  nextDay

            }
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}
```
### Operators overloading
```
import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate = addTimeIntervals(timeInterval, 1)

class RepeatedTimeInterval(val timeInterval: TimeInterval, val number: Int)

operator fun TimeInterval.times(number: Int) =
        RepeatedTimeInterval(this, number)

operator fun MyDate.plus(timeIntervals: RepeatedTimeInterval) =
        addTimeIntervals(timeIntervals.timeInterval, timeIntervals.number)


fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}
```
### Invoke
```
class Invokable {
    var numberOfInvocations: Int = 0
        private set

    operator fun invoke(): Invokable {
       numberOfInvocations++
        return   this
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
```
